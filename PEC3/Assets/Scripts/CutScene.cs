using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CutScene : MonoBehaviour
{
    private Scene actualScene;
    private string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        actualScene = SceneManager.GetActiveScene();
        sceneName = actualScene.name;

        StartCoroutine(NextLevel());
    }

    IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(2);

        if (sceneName == "Stage1")
            SceneManager.LoadScene("Level1");

        else if (sceneName == "Stage2")
            SceneManager.LoadScene("Level2");

        else if (sceneName == "FinalStage")
            SceneManager.LoadScene("FinalLevel");

        else if (sceneName == "GameOver")
        {
            GameObject.FindGameObjectWithTag("BSO").GetComponent<SoundManager>().StopMusic();
            SceneManager.LoadScene("NewGame");
        }
      
    }
}
