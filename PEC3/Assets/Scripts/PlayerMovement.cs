using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private SoundManager soundManager;
    private AudioSource bso;

    private Vector2 direction;
    private Vector3 colliderOffset;
    private LayerMask groundLayer;

    private bool isOnGround = false;
    private bool facingRight = true;

    private float speed = 10f;
    private float maxSpeed = 5f;
    private float jumpPower = 6f;
    private bool isJumping;
    private float jumpTimer;
    private float jumpTime = 0.4f;
    private float rayLenght = 0.6f;


    private float gravity = 1f;
    private float fallPower = 5f;
    private float linearDrag = 2f;

    // Start is called before the first frame update
    void Start()
    {
        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        groundLayer = LayerMask.GetMask("Ground");
        colliderOffset.x = 0.25f;
    }

    // Update is called once per frame
    void Update()
    {
        bool wasOnGound = isOnGround;

        direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        
        RaycastHit2D groundCheckLeft = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, rayLenght, groundLayer);
        RaycastHit2D groundCheckRight = Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, rayLenght, groundLayer);

        isOnGround = groundCheckLeft || groundCheckRight;

        if (!wasOnGound && isOnGround)
            gameObject.GetComponent<Animator>().SetBool("Jump", false);

        if (Input.GetKeyDown(KeyCode.UpArrow) && isOnGround)
        {
            isJumping = true;
            bso.PlayOneShot(soundManager.jump);
            jumpTimer = jumpTime;
            Jump();
        }

        if (Input.GetKey(KeyCode.UpArrow) && isJumping == true)
        {
            if (jumpTimer > 0)
            {
                Jump();
                jumpTimer -= Time.deltaTime;
            }
            else
            {
                isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            isJumping = false;
        }

    }

    private void FixedUpdate()
    {
        Move(direction.x);
        ModifyPhysics();
    }

    void Move(float xDir)
    {
        rb.velocity = new Vector2(xDir * speed, rb.velocity.y);
        GetComponent<Animator>().SetFloat("Speed", Mathf.Abs(rb.velocity.x));

        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);

        // Direction flip

        if (xDir < 0.0f && facingRight)
            Flip();
        else if (xDir > 0.0f && !facingRight)
            Flip();
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        gameObject.GetComponent<Animator>().SetBool("Jump", true);
    }

    void Flip()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
    }

    void ModifyPhysics()
    {
        if (!isOnGround)
        {
            rb.gravityScale = gravity;
            rb.drag = linearDrag * 0.5f;

            if (rb.velocity.y < 0)
            {
                rb.gravityScale = gravity * fallPower;
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                rb.gravityScale = gravity * (fallPower * 0.2f);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * rayLenght);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * rayLenght);
        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.up * rayLenght);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.up * rayLenght);
    }

}
