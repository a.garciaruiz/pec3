using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Slider healthSlider;

    // Start is called before the first frame update
    void Start()
    {
        healthSlider = gameObject.GetComponent<Slider>();
    }

    public void SetMaxHealth(int health)
    {
        healthSlider.value = health;
    }
    public void SetHealth(int health)
    {
        healthSlider.value = health;
    }
}
