using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUnit : MonoBehaviour
{

    private int maxHealth = 100;
    public int currentHealth;

    private HealthBar healthBar;
    private Animator animator;
    private Rigidbody2D rb;

    public int enemiesLeft = 0;
    private GameObject[] enemies;

    private Scene actualScene;
    private string sceneName;

    private SoundManager soundManager;
    private AudioSource bso;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        healthBar = gameObject.GetComponentInChildren<HealthBar>();
        animator = gameObject.GetComponent<Animator>();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        actualScene = SceneManager.GetActiveScene();
        sceneName = actualScene.name;

        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
    }

    // Update is called once per frame
    void Update()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        enemiesLeft = enemies.Length;

        if(sceneName == "FinalLevel")
        {
            enemies = GameObject.FindGameObjectsWithTag("Boss");
            enemiesLeft = enemies.Length;
        }
 
        if (currentHealth == 0 && gameObject.tag == "Enemy")
        {
            bso.PlayOneShot(soundManager.die);
            rb.velocity = new Vector2(0, -5);
            gameObject.GetComponent<EnemyMovement>().enabled = false;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            animator.Play("Died");
            Destroy(gameObject, 3f);
            enemiesLeft--;

            if (enemiesLeft == 0)
                StartCoroutine(NextLevel());
        }
        else if(currentHealth == 0 && gameObject.tag == "Boss")
        {
            bso.PlayOneShot(soundManager.die);
            rb.velocity = new Vector2(0, -5);
            gameObject.GetComponent<BossMovement>().enabled = false;
            animator.Play("Died");
            Destroy(gameObject, 3f);
            enemiesLeft--;

            if(enemiesLeft == 0)
                StartCoroutine(EndGame());
        }
        else if (currentHealth == 0 && gameObject.tag == "Player")
        {
            bso.PlayOneShot(soundManager.die);
            StartCoroutine(GameOver());
        }
    }

    IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(2);

        if (sceneName == "Level1")
            SceneManager.LoadScene("Stage2");
        else if (sceneName == "Level2")
            SceneManager.LoadScene("FinalStage");
    }

    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2);

        if (sceneName == "FinalLevel")
            SceneManager.LoadScene("GameOver");
    }

    IEnumerator GameOver()
    {
        gameObject.GetComponent<PlayerMovement>().enabled = false;
        animator.Play("Died");

        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(2f);

        SceneManager.LoadScene("GameOver");

        Time.timeScale = 1;
    }

    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;
        healthBar.SetHealth(currentHealth);
    }
}
