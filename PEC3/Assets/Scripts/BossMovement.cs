using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private GameObject player;
    public Transform firePoint;
    public Transform bulletPrefab;

    private SoundManager soundManager;
    private AudioSource bso;

    private Vector3 colliderOffset;
    private LayerMask groundLayer;

    private float speed = 3f;
    private float jumpHeight = 100f;
    [SerializeField] private float rayLenght = 1.1f;

    public float distanceFromPlayer;
    private float attackDistance = 10f;
    private float retreatDistance = 7f;
    private float stopDistance = 3f;

    private bool facingRight = true;
    [SerializeField] private bool isOnGround = false;

    private float shotInterval;
    private float startShotInterval;

    private float jumpInterval;
    private float startJumpInterval;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");

        groundLayer = LayerMask.GetMask("Ground");
        colliderOffset.x = 0.25f;

        startShotInterval = Random.Range(1f, 3f);
        shotInterval = startShotInterval;

        startJumpInterval = Random.Range(1f, 2f);
        jumpInterval = startJumpInterval;

        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
    }

    private void Update()
    {
        bool wasOnGround = isOnGround;

        distanceFromPlayer = Vector2.Distance(player.transform.position, transform.position);

        RaycastHit2D groundCheckLeft = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, rayLenght, groundLayer);
        RaycastHit2D groundCheckRight = Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, rayLenght, groundLayer);

        isOnGround = groundCheckLeft || groundCheckRight;

        if (!wasOnGround && isOnGround)
            GetComponent<Animator>().SetBool("Jump", false);
    }

    private void FixedUpdate()
    {
        Move();

        if (distanceFromPlayer <= retreatDistance && isOnGround)
        {
            Jump();
        }
    }

    void Move()
    {
        if (distanceFromPlayer < stopDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
            GetComponent<Animator>().SetFloat("Speed", speed);
        }
        else if (distanceFromPlayer < attackDistance)
        {
            FlipTowardsPlayer();
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            GetComponent<Animator>().SetFloat("Speed", speed);
            Shoot();
        }
        else if(distanceFromPlayer > attackDistance)
        {
            transform.position = this.transform.position;
            GetComponent<Animator>().SetFloat("Speed", 0);
        }
    }

    void Jump()
    {
        if (jumpInterval <= 0)
        {
            float jumpDistance = player.transform.position.x - transform.position.x;
            bso.PlayOneShot(soundManager.jump);
            GetComponent<Animator>().SetBool("Jump", true);
            rb.AddForce(new Vector2(jumpDistance, jumpHeight), ForceMode2D.Impulse);
            jumpInterval = startJumpInterval;
        }
        else
        {
            jumpInterval -= Time.deltaTime;
        }
    }

    void FlipTowardsPlayer()
    {
        if (transform.position.x < player.transform.position.x && facingRight)
            Flip();
        else if (transform.position.x > player.transform.position.x && !facingRight)
            Flip();
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
    }

    void Shoot()
    {
        if (shotInterval <= 0)
        {
            bso.PlayOneShot(soundManager.shoot);
            GetComponent<Animator>().SetBool("Shoot", true);
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            shotInterval = startShotInterval;
        }
        else
        {
            GetComponent<Animator>().SetBool("Shoot", false);
            shotInterval -= Time.deltaTime;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * rayLenght);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * rayLenght);

        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.right * rayLenght);
    }
}
