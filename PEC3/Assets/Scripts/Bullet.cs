using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float speed = 20f;
    private int bulletDamage = 50;
    private Rigidbody2D rb;
    private Animator bulletAnim;

    private SoundManager soundManager;
    private AudioSource bso;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        bulletAnim = GetComponent<Animator>();

        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        bso.PlayOneShot(soundManager.hit);

        if (collision.gameObject.tag != "Player")
        {
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = 0f;
            bulletAnim.Play("Hit");
            Destroy(gameObject, 0.2f);
        }

        if (collision.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<PlayerUnit>().TakeDamage(bulletDamage);
        }

        if (collision.CompareTag("Boss"))
        {
            collision.gameObject.GetComponent<PlayerUnit>().TakeDamage(bulletDamage/10);
        }
    }
}