using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private GameObject player;
    public Transform firePoint;
    public GameObject bulletPrefab;

    private SoundManager soundManager;
    private AudioSource bso;

    private Vector3 colliderOffset;
    private LayerMask groundLayer;
    private LayerMask wallLayer;

    private float speed = 2f;
    private float step;
    private float jumpPower = 7f;
    private float rayLenght = 0.6f;

    private float dir = 1;
    private float distanceFromPlayer;
    private float attackDistance = 10f;
    private float retreatDistance = 5f;

    private bool facingRight = true;
    private bool isOnGround = false;
    private bool isOnWall = false;

    private float shotInterval;
    private float startShotInterval;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        groundLayer = LayerMask.GetMask("Ground");
        wallLayer = LayerMask.GetMask("Wall");
        colliderOffset.x = 0.25f;

        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();

        startShotInterval = Random.Range(1f, 3f);
        shotInterval = startShotInterval;
    }

    private void Update()
    {
        bool wasOnGround = isOnGround;

        distanceFromPlayer = player.transform.position.x - transform.position.x;
        step = speed * Time.deltaTime;

        RaycastHit2D groundCheckLeft = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, rayLenght, groundLayer);
        RaycastHit2D groundCheckRight = Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, rayLenght, groundLayer);

        RaycastHit2D wallCheck = Physics2D.Raycast(transform.position + colliderOffset, Vector2.right, rayLenght, wallLayer);

        isOnGround = groundCheckLeft || groundCheckRight;
        isOnWall = wallCheck;

        if (!wasOnGround && isOnGround)
            GetComponent<Animator>().SetBool("Jump", false);
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(distanceFromPlayer) < attackDistance)
        {
            FlipTowardsPlayer();
            rb.velocity = new Vector2(dir * speed, rb.velocity.y);
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, step);
            Shoot();
            GetComponent<Animator>().SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        }
        else if (Mathf.Abs(distanceFromPlayer) > attackDistance)
                Move();

        if (Mathf.Abs(distanceFromPlayer) < retreatDistance)
        {
            rb.velocity = new Vector2(dir * -speed, rb.velocity.y);
            Shoot();
            GetComponent<Animator>().SetFloat("Speed", 0);
        }


        if (isOnWall == true)
        {
            Flip();
        }
    }

    void Move()
    {
        rb.velocity = new Vector2(dir * speed, rb.velocity.y);
        GetComponent<Animator>().SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }

    void Jump()
    {
        bso.PlayOneShot(soundManager.jump);
        GetComponent<Animator>().SetBool("Jump", true);
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
    }

    void FlipTowardsPlayer()
    {
        if (distanceFromPlayer < 0 && facingRight)
            Flip();
        else if (distanceFromPlayer > 0 && !facingRight)
            Flip();
    }

    void Flip()
    {
        dir *= -1;
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
        isOnWall = false;
    }

    void Shoot()
    {
        if (shotInterval <= 0)
        {
            bso.PlayOneShot(soundManager.shoot);
            GetComponent<Animator>().SetBool("Shoot", true);
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            shotInterval = Random.Range(1f, 3f);
        }
        else
        {
            GetComponent<Animator>().SetBool("Shoot", false);
            shotInterval -= Time.deltaTime;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (Mathf.Abs(distanceFromPlayer) > attackDistance)
        {
            Flip();
        }
        else if (Mathf.Abs(distanceFromPlayer) < attackDistance && isOnGround)
            Jump();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * rayLenght);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * rayLenght);

        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.right * rayLenght);
    }
}
