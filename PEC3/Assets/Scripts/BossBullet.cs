using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullet : MonoBehaviour
{
    private float speed = 5f;
    private int bulletDamage = 25;
    private Rigidbody2D rb;
    private Animator bulletAnim;

    private GameObject player;
    private Vector2 target;

    private SoundManager soundManager;
    private AudioSource bso;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        bulletAnim = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player");
        target = new Vector2(player.transform.position.x, player.transform.position.y);

        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
    }

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (transform.position.x == target.x && transform.position.y == target.y)
            DestroyProjectile();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerUnit>().TakeDamage(bulletDamage);
        }
    }

    void DestroyProjectile()
    {
        bso.PlayOneShot(soundManager.hit);
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = 0f;
        bulletAnim.Play("Hit");
        Destroy(gameObject, 0.2f);
    }
}
