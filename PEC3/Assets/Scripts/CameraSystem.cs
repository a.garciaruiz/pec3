using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraSystem : MonoBehaviour
{
    public Transform target;

    private float smoothSpeed = 1f;

    //Initial values
    public Vector3 offset;
    private float xPos = 6f;
    private float yPos = 3f;
    private float zPos = -10f;

    //Boundaries
    public Vector3 minValues, maxValues;

    private Scene actualScene;
    private string sceneName;

    private void Start()
    {
        actualScene = SceneManager.GetActiveScene();
        sceneName = actualScene.name;

        if (sceneName == "FinalLevel")
        {
            maxValues.x = 13.5f;
        }

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            Flip();
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            offset = new Vector3(xPos, yPos, zPos);
    }

    private void LateUpdate()
    {
        Vector3 targetPos = target.position + offset;

        Vector3 boundPos = new Vector3(
            Mathf.Clamp(targetPos.x, minValues.x, maxValues.x),
            Mathf.Clamp(targetPos.y, minValues.y, maxValues.y),
            Mathf.Clamp(targetPos.z, minValues.z, maxValues.z)
            );

        Vector3 smoothPos = Vector3.Lerp(transform.position, boundPos, smoothSpeed * Time.fixedDeltaTime);
        transform.position = smoothPos;

    }

    void Flip()
    {
        offset = new Vector3(-xPos, yPos, zPos);
    }
}
