using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    private Animator playerAnim;

    private SoundManager soundManager;
    private AudioSource bso;

    private void Start()
    {
        playerAnim = gameObject.GetComponent<Animator>();
        bso = GameObject.FindGameObjectWithTag("BSO").GetComponent<AudioSource>();
        soundManager = bso.GetComponent<SoundManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Shoot();

        if (Input.GetKeyUp(KeyCode.Space))
            playerAnim.SetBool("Shoot", false);
    }

    public void Shoot()
    {
        bso.PlayOneShot(soundManager.shoot);
        playerAnim.SetBool("Shoot", true);
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}
